#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = nullptr);
    ~Dialog();

private:
    Ui::Dialog *ui;
    QStringList m_commands;
    int commands_index;

protected:
    bool eventFilter(QObject *obj, QEvent *event);//in Dialog header
};

#endif // DIALOG_H
