#include "dialog.h"
#include "ui_dialog.h"
#include <QStandardPaths>
#include <QProcess>
#include <QKeyEvent>

QString createNewText(const QString& command, const QStringList& arguments, const QString& message) {

    QString text = command;

    if (arguments.length() > 0) {
        text += " Arguments: ";
        for (auto argument: arguments)
            text += argument + ",";
    }
    text += "\n";
    text += message + "\n\n";
    return text;
}

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    ui->lversion->setText("V2");

    connect(ui->leInput, &QLineEdit::returnPressed, [this] () {
        const QString input = ui->leInput->text();
        ui->leInput->clear();
        m_commands.append(input);
        commands_index = 0;
        const QString bash = QStandardPaths::findExecutable("bash");

        QProcess p(this);
        p.setProcessChannelMode(QProcess::MergedChannels);

        QStringList arguments;
        arguments.append("-c");
        arguments.append(input);

        const QString command = bash; //input;

        p.start(command, arguments);

        QString message;
        if (!p.waitForFinished()) {
            // failed
            message = p.errorString();
        } else {
            // success
            message = p.readAll();
        }

        ui->teOutput->setText(ui->teOutput->toPlainText() + createNewText(command, arguments, message));
    });

    qApp->installEventFilter(this);
}

bool Dialog::eventFilter(QObject *obj, QEvent *event)
{
    if (obj == ui->leInput && event->type() == QEvent::KeyPress)
    {
        QKeyEvent *key = static_cast<QKeyEvent *>(event);
        if (m_commands.count() > 0) {
            // load last command
            if (key->key() == Qt::Key_Up) {
                ui->leInput->setText(m_commands.at(m_commands.count() - 1 - commands_index));
                commands_index++;
                if (commands_index > m_commands.count() - 1)
                    commands_index = m_commands.count() - 1;
            } else if (key->key() == Qt::Key_Down) {
                ui->leInput->setText(m_commands.at(m_commands.count() - 1 - commands_index));
                commands_index--;
                if (commands_index < 0)
                    commands_index = 0;
            }
        }
    }
    return QObject::eventFilter(obj, event);
}

Dialog::~Dialog()
{
    delete ui;
}
