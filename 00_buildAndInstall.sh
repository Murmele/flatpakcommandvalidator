./build.sh
flatpak build-bundle FlatpakCommandValidatorRepo FlatpakCommandValidator.flatpak com.gitlab.Murmele.FlatpakCommandValidator --runtime-repo=https://flathub.org/repo/flathub.flatpakrepo
flatpak install -y --user ./FlatpakCommandValidator.flatpak
